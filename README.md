# Server check list:

* Check that you can login to the GUI of the server
* Check that you can browse internet through browser in GUI


* Swith to `root` (admin user) using sudo:

(type in password of the GUI admin user)

```
sudo -i
```

* Check the IP of the server:

```
ip addr show
```

* Check if name resolution is working:

```
dig yahoo.com
```

* Ping a remote server on the internet:

```
ping google.com
```

* Check if SSH service is running:

```
systemctl status sshd
```

* Enable SSHD:

```
systemctl enable sshd

systemctl restart sshd
```

* Check if firewall is off - it should be off:

```
systemctl status firewalld
```

* Stop the firewall and disable it permanently:

```
systemctl stop firewalld

systemctl disable firewalld

yum -y remove firewalld
```

* Check running services:

```
netstat -ntlp
```


